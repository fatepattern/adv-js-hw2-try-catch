const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

let element = document.getElementById("root");

let ul = document.createElement('ul');

element.appendChild(ul);

let bookWithFullInfo = [];

let objectKeys = [];

let uniqueKeys = [];

books.forEach((book) => {
    if(book.hasOwnProperty('author') && book.hasOwnProperty('name') && book.hasOwnProperty('price')){
        bookWithFullInfo.push(book);
    }

    let keys = Object.keys(book);
    
    objectKeys = objectKeys.concat(keys);

    uniqueKeys = [...new Set(objectKeys)];

    for(let i = 0; i < uniqueKeys.length; i++){

        try{
            if(!book.hasOwnProperty(uniqueKeys[i])){
                throw new TypeError("field " + uniqueKeys[i] + " is missing");
            }
        } catch(e){
            console.log(e.message);
        }
    }

    
})

for(let i = 0; i < bookWithFullInfo.length; i++){
    const li = document.createElement('li');
    li.innerText = bookWithFullInfo[i].author + ";" + bookWithFullInfo[i].name + ";" + bookWithFullInfo[i].price;
    ul.appendChild(li);
}
